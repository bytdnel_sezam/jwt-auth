import ContactEntity from '../../../entities/contact.entity';
import { ContactsController } from './contacts.controller';
import { ContactsService } from './contacts.service';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([ContactEntity]),
  ],
  providers: [ContactsService],
  controllers: [ContactsController]
})
export class ContactsModule {}
