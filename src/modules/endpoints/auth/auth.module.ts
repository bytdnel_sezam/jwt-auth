import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import BcryptModule from 'modules/services/bcryptjs/bcryptjs.module';
import { JwtModule } from '@nestjs/jwt';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import UserEntity from '../../../entities/user.entity';
import { UserService } from './user.service';

@Module({
    imports: [
        BcryptModule,
        TypeOrmModule.forFeature([UserEntity]),
        JwtModule.register({
            secretOrPrivateKey: 'secret12356789'
            
        })
    ],
    providers: [UserService, AuthService],
    controllers: [AuthController]
})
export class AuthModule {}
