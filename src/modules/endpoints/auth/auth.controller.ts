import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import UserEntity from '../../../entities/user.entity';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}
    
    @Post('login')
    async login(@Body() user: UserEntity): Promise<any> {
      return this.authService.login(user);
    }  

    @Post('register')
    async register(@Body() user: UserEntity): Promise<any> {
      return this.authService.register(user);
    }  
}
