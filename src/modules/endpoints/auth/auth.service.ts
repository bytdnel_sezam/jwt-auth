import BcryptService from 'modules/services/bcryptjs/bcryptjs.service';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import UserEntity from '../../../entities/user.entity';
import { UserService } from './user.service';
import { access } from 'fs';

interface AuthCredentials {
    email: string;
    password: string;
}

// enum TimeInMs {
//     Hours24 = 24*60*60*1000,
// }

@Injectable()
export class AuthService {
    constructor(
        private readonly userService: UserService,
        private readonly jwtService: JwtService,
        private readonly bcryptService: BcryptService,
    ) { }

    private async _validateEmail(email: string): Promise<UserEntity | null> {
        const user = await this.userService.findByEmail(email);
        if (user) return user;
        else return null;
    }
    
    private async _validateUser(credentials: AuthCredentials) {
        const authenticatedUser = await this.userService.findByEmail(credentials.email);
        const passwordSuitable = await this.bcryptService.compare(
            credentials.password,
            authenticatedUser.password
        );
        if (authenticatedUser && passwordSuitable) return authenticatedUser;
        else return false;
    }

    private async _getAccessToken(user: UserEntity): Promise<string> {
        const userStringified = JSON.stringify(user);
        const accessToken = await this.jwtService.signAsync(userStringified);
        return accessToken;
    }
    
    // private async decodeJWT(accessToken: string) {
    //     return await this.jwtService.verifyAsync(accessToken);
    // }

    public async login(credentials: AuthCredentials): Promise<any | { status: number }>{
        const userInstance = await this._validateUser(credentials);
        if (userInstance) {
            const accessToken = await this._getAccessToken(userInstance);
            return {
                access_token: accessToken,
                status: 200
            };
        } else {
            return { status: 404 };
        }
    }

    public async register(user: UserEntity): Promise<any>{
        const userInstance = await this._validateEmail(user.email);
        console.log({userInstance});
        if (userInstance) {
            return {
                status: 500
            }
        } else {
            const hashedPassword = await this.bcryptService.hash(user.password);
            user.password = hashedPassword;
            const userCreated = await this.userService.create(user);
            const accessToken = await this._getAccessToken(userCreated);
            return {
                access_token: accessToken,
                status: 200
            };
        }
        
    }
}
