import { AuthModule } from '../endpoints/auth/auth.module';
import ContactEntity from '../../entities/contact.entity';
import { ContactsModule } from '../endpoints/contacts/contacts.module';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import UserEntity from '../../entities/user.entity';

const PostgreSQLConnection = TypeOrmModule.forRoot({
  type: 'postgres',
  username: 'danielbyta',
  password: '',
  port: 5432,
  database: 'smallmall',
  entities: [UserEntity, ContactEntity],
  synchronize: true,
})

const MongoDBConnection = TypeOrmModule.forRoot({
  type: 'mongodb',
  url: process.env.MONGODB_CONNECTION_STRING,
  database: process.env.MONGODB_DATABASE,
  entities: [
    __dirname + '/**/*.entity{.ts,.js}',
  ],
  ssl: true,
  useUnifiedTopology: true,
  useNewUrlParser: true
})

@Module({
  imports: [
    ContactsModule,       
    PostgreSQLConnection,
    AuthModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
