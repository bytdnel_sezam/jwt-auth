import * as bcrypt from 'bcryptjs'

import { Injectable } from "@nestjs/common";

@Injectable()
export default class BcryptService {
  private saltRounds = 10;

  constructor() {}

  private async genSalt(): Promise<string> {
    return bcrypt.genSalt(this.saltRounds);
  }

  async hash(payload: string): Promise<string> {
    console.log('in hash');
    console.log({bcrypt});
    return await bcrypt.hash(
      payload, 
      (await this.genSalt())
    );
  }

  async compare(payload: string, hash: string): Promise<boolean> {
    return bcrypt.compare(payload, hash);
  } 
}