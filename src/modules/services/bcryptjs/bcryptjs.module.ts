import BcryptService from "./bcryptjs.service";
import { Module } from "@nestjs/common";

@Module({
  imports: [],
  controllers: [],
  providers: [BcryptService],
  exports: [BcryptService]
})
export default class BcryptModule {}